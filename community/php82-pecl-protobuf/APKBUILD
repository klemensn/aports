# Contributor: TBK <alpine@jjtc.eu>
# Maintainer: TBK <alpine@jjtc.eu>
pkgname=php82-pecl-protobuf
_extname=protobuf
pkgver=3.23.3
pkgrel=0
pkgdesc="PHP 8.2 extension: Google's language-neutral, platform-neutral, extensible mechanism for serializing structured data."
url="https://pecl.php.net/package/protobuf"
arch="all"
license="BSD-3-Clause"
_phpv=82
_php=php$_phpv
depends="$_php-common"
makedepends="$_php-dev"
source="php-pecl-$_extname-$pkgver.tgz::https://pecl.php.net/get/$_extname-$pkgver.tgz"
builddir="$srcdir/$_extname-$pkgver"
provides="php7-protobuf=$pkgver-r$pkgrel" # for backward compatibility
replaces="php7-protobuf" # for backward compatibility

build() {
	phpize$_phpv
	./configure --prefix=/usr --with-php-config=php-config$_phpv
	make
}

check() {
	# Test suite is not a part of pecl release.
	$_php -d extension=modules/$_extname.so --ri $_extname
}

package() {
	make INSTALL_ROOT="$pkgdir" install
	local _confdir="$pkgdir"/etc/$_php/conf.d
	install -d $_confdir
	echo "extension=$_extname" > $_confdir/$_extname.ini
}

sha512sums="
35055daf97c3ce5a5d4319af418620378a2d372312413b48a57f3911f904dca5622f59438dad630decfdfce596e6a1fca1b143c7701257240527a735a0caca51  php-pecl-protobuf-3.23.3.tgz
"
